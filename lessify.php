#!/usr/bin/php
<?php

require 'lessphp/lessc.inc.php';

try {
    lessc::ccompile('less/main.less', 'web/css/main.css');
} catch (exception $ex) {
    exit('lessc fatal error:'."\n".$ex->getMessage()."\n\n\n");
}

?>