<?php

require_once '../config.php';

$r = '';

if($_GET['type'] == 'css')
{
	$content = 'text/css';
	$ext = 'css';
}
elseif($_GET['type'] == 'js')
{
	$content = 'application/javascript';
	$ext = 'js';
}

header('Content-type: '.$content.'; charset=UTF-8');

if(isset($ext))
{
	foreach(explode(',', $_GET['files']) as $k => $file)
	{
		$fullPath = $ext.'/'.$file.'.'.$ext;
//		echo $fullPath;exit;
		if(file_exists($fullPath))
		{
			if($k != 0)
				$r .= "\n\n";
			
			//$r .= '/*[-----<[ '.$file.' ]>-----]*/'."\n\n";
			$content = file_get_contents($fullPath);
			$content = str_replace('{assetsPath}', ASSETS_PATH, $content);
			$r .= $content;
		}
		else
		{
			//$r .= "\n\n".'/*[-----<[ '.$file.' : NOT FOUND ]>-----]*/'."\n\n";
		}
	}

	if(!empty($r))
	{
		echo $r;
	}
}