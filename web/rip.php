<?php

require_once '../config.php';

$pages = array(
	'presentation',
	'services',
	'competences',
	'realisations',
	'mentions-legales',
);

$menu = array(
	array('nom' => 'presentation', 'titre' => 'Présentation', 'title' => ''),
	array('nom' => 'services', 'titre' => 'Services', 'title' => 'Services : Création de sites'),
	array('nom' => 'competences', 'titre' => 'Compétences', 'title' => 'Compétences : Prestataire Symfony'),
	array('nom' => 'realisations', 'titre' => 'Réalisations', 'title' => 'Réalisations : Exemples de projets'),
	array('nom' => 'mentions-legales', 'titre' => 'Mentions légales', 'hidden' => true)
);

$pageActuelle = $menu[0]['nom'];
$titrePageActuelle = $menu[0]['titre'];
$titlePageActuelle = $menu[0]['title'];

foreach($menu as $item)
{
	if(isset($_GET['p']) && $_GET['p'] == $item['nom'].'.html')
	{
		$pageActuelle = $item['nom'];
		$titrePageActuelle = $item['titre'];
		$titlePageActuelle = $item['title'];
		break;
	}
}

$ajax = false;

//var_dump($_GET['p'], $_SERVER);exit;

if(isset($_GET['p']))
{
	$page = pathinfo($_GET['p']);
	$page = $page['filename'];
	
	if(preg_match('/(.+).ajax$/', $page, $match))
	{
		$ajax = true;
		$page = $match[1];
	}
}

if(!isset($page))
{
	$page = $pages[0];
	$canonical = $page.'.html';
}
elseif(!in_array($page, $pages))
{
	header("HTTP/1.1 301 Moved Permanently");
	header ('location: /');
	header("Connection: close");
}


function loadPage($page, $ajax = false)
{
	$page = '../pages/'.$page.'.php';
	file_exists($page) ? include($page) : print('Page introuvable');
}

if($ajax === true)
{
	loadPage($page, $ajax);
	exit;
}

function asset($path)
{
	return ASSETS_PATH.$path;
}

require_once '../pages/layout.php';

?>
