$(function(){
	// Menu (bug)
	if(!$.browser.msie || $.browser.version != '6.0')  {
		
		$('ul#menu li').each(function(){
			var background = '<div class="background" />';
			var backgroundOver = '<div class="background-over" />';
			var backgroundCurrent = '<div class="background-current" />';
			var a = $(this).children('a');

			if(!a.hasClass('current')){
				a.before(background).before(backgroundOver);
			} else {
				a.before(backgroundCurrent)
			}
			
			a.hover(function(){
				target = $(this).siblings('.background-over');

				if(target.queue("fx").length <= 1){
					target.addClass('on');
					target.fadeIn();
				}
				
			}, function(){
				target = $(this).siblings('.background-over');

				if(target.queue("fx").length <= 1){
					target.removeClass('on');
					target.fadeOut();
				}
			});
		});
	}
	
	// ANTI SP4M
	at = '@';
	domain = 'easytek.fr';
	$('a[href^="mailto"]').each(function(){
		$(this).text($(this).text()+at+domain);
		$(this).attr('href', $(this).attr('href')+at+domain);
	});
	
	// DIAPO
	$('#diapo .slides').cycle({fx: 'fade'});
	
	// SOUS HEADER
	$('#sous-header').children().hide().fadeIn();
	
	// REALISATIONS (bug)
	var inside = $('.realisation');

	inside.hover(
		function(){
			var target = $(this).find(".infos");
			
			if(!inside.data('bottom')){
				inside.data('bottom', target.css('bottom'));
			}
			
			if(target.css('bottom') == $(this).data('bottom') && target.queue("fx").length <= 1) {
				target.animate({bottom:'+=150px'}, "normal");
			}
		}, function(){
			var target = $(this).find(".infos");
			
			if(target.queue("fx").length <= 1){
				target.animate({bottom:$(this).data('bottom')}, "normal");
			}
	});
	
	// MENTIONS LEGALES
	$('#popin').jqm({ajax: 'mentions-legales.ajax.html', trigger: 'a#mentions-legales'});
	
	// REALISATION
	$('#realisations').find('img').each(function(){
		var url = 'http://'+$(this).attr('alt').toLowerCase()+'/';
		$(this).parent('div').find('h3').wrap('<a href="'+url+'" />');
	});
});