<div id="sous-header">
	<img src="<?=asset('images/icone-competences.png')?>" alt="icone" />
	<h1>Une expertise pointue dans les technologies web. </h1>
	<p>Nous travaillons principalement sur des technologies basées sur PHP et nous nous efforçons de maintenir une qualité optimale de développement grâce à l’emploi de frameworks réputés, tels que Symfony, dans lequel nous sommes spécialisés. De plus, nous maintenons une veille continue nous permettant de proposer des technologies web modernes telles que Ajax via jQuery, HTML5 et CSS3.</p>
</div>
<div id="content">
	<?php loadPage('_nomoreie'); ?>
	
	<div class="competences">
		<div class="competence">
			<h2>Languages</h2>
			<p>
				PHP est le langage de programmation de référence pour une grande partie des sites web dynamiques sur Internet. Utilisé conjointement avec HTML&nbsp;5, CSS&nbsp;3 et Javascript, il offre une grande souplesse dans le développement d’applications web modernes.<br />
				Notre volonté de qualité se retrouve également dans nos intégrations de webdesign en xHTML et CSS, pour lesquelles nous pouvons appliquer les normes d’accessibilité du W3C.<br />
				Nous pouvons sur demande réaliser des développements spécifiques en Python.	
			</p>
			<img src="<?=asset('images/competences-languages.png')?>" alt="PHP HTML5 CSS JS Python" />
		</div>
		<div class="competence">
			<h2>Frameworks</h2>
			<p>
				Les frameworks permettent de normaliser et accélérer les développements. Ils s’appuient sur des composants logiciels et une méthodologie éprouvés, les applications web gagnent ainsi en robustesse et leur maintenance se voit grandement améliorée.<br />
				Nous avons fait du framework PHP français Symfony notre spécialité. Basé sur la programmation objet et le design pattern MVC, nous l’utilisons dans ses version 2 et 1.4. Nos développements intègrent également le framework Javascript jQuery. Pour des besoins particuliers nous pouvons également intervenir sur le framework Code Igniter.
			</p>
			<img src="<?=asset('images/competences-frameworks.png')?>" alt="Symfony 2 et jQuery" />
		</div>
		<div class="competence">
			<h2>Serveurs</h2>
			<p>
				Pour héberger les sites de nos clients, nous utilisons des serveurs Linux Debian et principalement le serveur web Apache et le serveur de bases de données MySQL. Pour plus de souplesse et de rapidité d’évolution, nous exploitons la virtualisation OpenVZ via l’hyperviseur de machines virtuelles Proxmox.
			</p>
			<img src="<?=asset('images/competences-serveurs.png')?>" alt="Linux Debian Apache MySQL Proxmox" />
		</div>
	</div>
</div>