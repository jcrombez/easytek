<div id="sous-header">
	<img src="<?=asset('images/icone-presentation.png')?>" alt="icone" />
	<h1>Votre agence web sur Grenoble.</h1>
	<p>S'efforcant de maintenir une expertise pointue dans les technologies web, <strong>Easytek</strong> prend en charge les projets de votre entreprise des plus simples aux plus ambitieux.</p>
</div>
<div id="content">
	<?php loadPage('_nomoreie'); ?>
	
	<div id="diapo">
		<div class="slides">
			<img src="<?=asset('images/realisations/omnt.png')?>" alt="OMNT" />
			<img src="<?=asset('images/realisations/lcvr45.png')?>" alt="LCVR45" />
			<img src="<?=asset('images/realisations/emd.png')?>" alt="EMD" />
		</div>
	</div>

	<h2>Qui sommes-nous ?</h2>
	<p><strong>Easytek</strong> est une entreprise située à Grenoble qui s'efforce de maintenir une veille technologique constante afin de vous proposer des solutions évolutives, modernes et innovantes (tel que HTML5, CSS3 ou Symfony 2).</p>
	<p>Nous vous accompagnons pendant tout le processus de réalisation et de suivi de votre projet de site ou d'application web.</p>

	<div class="contact">
		<div class="tel"><a href="tel:0482530277">04 82 53 02 77</a></div>
		<div class="email"><a href="mailto:contact">contact</a></div>
	</div>

	<div class="clear"></div>

	<div class="col1">
		<h3>Site vitrine &amp; sur mesure</h3>
		<p>Vous voulez gagner en visiblité avec un site vitrine, ou répondre à un besoin métier avec un site sur mesure ? Contactez-nous et discutons-en.</p>
		<a href="/realisations.html">Nos réalisations</a>
	</div>
	<div class="col2">
		<h3>Des solutions modernes</h3>
		<p>Applications smartphones, web mobile, réseaux sociaux, blog d’entreprise, nous vous proposons de nombreuses autres prestations.</p>
		<a href="/services.html">Plus de services</a>
	</div>
	<div class="col3">
		<h3>Notre spécialité : Symfony</h3>
		<p>Favorisant les bonnes pratiques de développement, le framework Symfony 2 (ou 1.4) nous permet d’allier qualité et efficacité.</p>
		<a href="/competences.html">Nos autres compétences</a>
	</div>

	<img src="<?=asset('images/logos.png')?>" alt="PHP Symfony Linux HTML5 jQuery Python" id="logos" />
</div>
