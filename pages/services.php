<div id="sous-header">
	<img src="<?=asset('images/icone-services.png')?>" alt="icone" />
	<h1>Découvrez notre gamme de services.</h1>
	<p><strong>Easytek</strong> prend en charge les projets web de votre entreprise, des plus simples aux plus ambitieux. Nous mettons nos compétences à votre disposition via une offre de services adaptés. Du simple blog au site sur mesure, du référencement à l’hébergement, nous avons une réponse pour chaque étape de votre projet.</p>
</div>
<div id="content">
	<?php loadPage('_nomoreie'); ?>
	
	<div class="services">
		<div class="service sur-mesure">
			<h2>Site sur mesure</h2>
			<p>Grâce à l’utilisation de frameworks comme Symfony, nous sommes en mesure de répondre aux besoins spécifiques de votre activité et de développer des fonctionnalités adaptées à votre métier.</p>
		</div>
		<div class="service vitrine">
			<h2>Site vitrine</h2>
			<p>Aujourd’hui devenu indispensable, le site vitrine est la carte d’identité de votre entreprise sur internet. Il vous permet de décrire votre entreprise à vos clients. Ce type de site est administrable facilement, vous permettant de modifier vous-même son contenu.</p>
		</div>
		<div class="service hebergement">
			<h2>Hébergement</h2>
			<p>Pour simplifier la gestion de votre site, nous vous proposons de l’héberger sur nos serveurs, et de n’avoir ainsi qu’un interlocuteur. Cela nous permet d’intervenir plus rapidement qu’avec un hébergeur tiers et d’effectuer des sauvegardes régulières de votre site web. </p>
		</div>
		<div class="service webdesign">
			<h2>Webdesign</h2>
			<p>Nous définissons ensemble le design de votre site web afin de le faire correspondre à l’image de votre entreprise. De la maquette à la page web, nous sommes là pour vous aider.</p>
		</div>
		<div class="service referencement">
			<h2>Référencement</h2>
			<p>Pour donner toutes les chances à votre site d’être bien classé dans les moteurs de recherche (tels que Google), nous l’optimisons pour un référencement naturel efficace.</p>
		</div>
		<div class="service blog">
			<h2>Blog professionel</h2>
			<p>De plus en plus plébiscité par les sociétés pour leur communication sur internet, le blog d’entreprises est un outil simple et abordable qui permet d’informer et de fidéliser les lecteurs.</p>
		</div>
		<div class="service social">
			<h2>Réseaux sociaux</h2>
			<p>Nous vous aidons à être présents sur les réseaux sociaux (tels que Facebook et Twitter) en vous accompagnant dans la création et l’animation d’un compte pour votre entreprise. Nous pouvons également vous proposer de relier votre site à ces réseaux.</p>
		</div>
		<div class="service smartphone">
			<h2>Site pour smartphone</h2>
			<p>L’avenir du web se joue sur les smartphones et le web mobile, ayez une longueur d’avance sur la concurence et proposez à vos visiteurs une version de votre site internet dédiée aux téléphones mobiles.</p>
		</div>
		<div class="service saas">
			<h2>Bureautique en ligne</h2>
			<p>Profitiez d'applications bureautiques intuitives et sécurisées : e-mail, agenda, traitement de texte, tableurs, présentations et messagerie instantanée. Nous vous accompagnons dans la mise en place de ces solutions SaaS basées sur le service  de cloud computing “Google Apps”.</p>
		</div>
		<div class="service email">
			<h2>Campagne d’e-mails</h2>
			<p>Dans le cadre de votre communication en ligne, nous réalisons vos campagnes d’e-mailing afin de communiquer auprès de vos clients par e-mail sous forme de newsletter ou sur un produit en particulier.</p>
		</div>
	</div>
</div>