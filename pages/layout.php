<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $titlePageActuelle != '' ? $titlePageActuelle.' | ' : ''; ?> Easytek | Agence Web Grenoble</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Easytek, agence web située à Grenoble, spécialisée Symfony, vous accompagne dans la réalisation de votre site internet ou vos besoins en application web." />
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<?php if (preg_match('/^Mozilla\/4\.0 \(compatible; MSIE 6/', $_SERVER['HTTP_USER_AGENT']) != 0): ?>
			<script type="text/javascript" src="/hub.php?type=js&files=jquery.cycle.all,jqModal,main"></script>
		<?php else: ?>
			<script type="text/javascript" src="/hub.php?type=js&files=jquery.cycle.all.min,jqModal.min,main.min"></script>
		<?php endif ?>
		<link rel="stylesheet" type="text/css" href="/hub.php?type=css&files=reset,main.min,jqModal<?=(isset($_GET['nocdn']))?'?nocdn':''?>" />
		<link rel="shortcut icon" href="/images/favicon.ico" />
		
		<? if(isset($canonical)): ?><link rel="canonical" href="http://<?=$_SERVER['HTTP_HOST']?>/<?=$canonical?>" /><? endif ?>
		
	</head>
	<body>
		<div id="main">
			<div id="header">
				<a href="/presentation.html" id="logo"><img src="<?=asset('images/logo.png')?>" alt="Easytek" /></a>
				<ul id="menu">
					<? foreach($menu as $item): ?>
						<?php if(!isset($item['hidden'])): ?>
							<li><a href="<?=BASE_URL?><?=$item['nom']?>.html" <?php if($item['nom'] == $pageActuelle): ?>class="current"<?php endif ?>><?=$item['titre']?></a></li>
						<?php endif ?>
					<? endforeach ?>
				</ul>
			</div>
			<?php loadPage($page); ?>
			<div id="popin" class="jqmWindow"></div>
			<div id="footer">
				<div class="wraper">
					<div class="infos">
						<h3>Easy<span>tek</span></h3>
						<p>
							RCS Grenoble 504 770 306<br />
							<a id="mentions-legales" href="/mentions-legales.html">Mentions légales</a>
						</p>
					</div>

					<ul id="sitemap">
					<? foreach($menu as $item): ?>
						<?php if(!isset($item['hidden'])): ?>
							<li><a href="<?=BASE_URL?><?=$item['nom']?>.html"><?=$item['titre']?></a></li>
						<?php endif ?>
					<? endforeach ?>
					</ul>

					<ul id="social">
						<li><a href="http://twitter.com/#!/Easytek" target="_blank"><img src="<?=asset('images/twitter.png')?>" alt="Twitter"/></a></li>
						<li><a href="https://www.facebook.com/pages/Easytek/170142893049764" target="_blank"><img src="<?=asset('images/facebook.png')?>" alt="Facebook" /></a></li>
						<?php // https://ssl.gstatic.com/images/icons/gplus-32.png ?>
						<li><a href="https://plus.google.com/b/115362264813255867471/115362264813255867471/about" target="_blank"><img src="<?=asset('images/gplus.png')?>" alt="Google Plus" /></a></li>
					</ul>

					<div id="etiquette">
						<h3>Contactez-nous</h3>
						<div class="tel"><a href="tel:0482530277">04 82 53 02 77</a></div>
						<div class="email"><a href="mailto:contact">contact</a></div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
	
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-929117-3']);
			_gaq.push(['_trackPageview']);
	
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
	
		</script>
	</body>
</html>
