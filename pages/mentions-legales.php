<?php if(!$ajax): ?><div id="sous-header"><?php endif ?>
	<h1>Mentions légales</h1>
	<p>L'éditeur du site Easytek.fr est Easytek SARL au capital de 1000 euros, son siège social est situé au 135 al des ancolies 38190 Villard Bonnot, et est immatriculée au Registre du Commerce et des Sociétés de Grenoble sous le numéro 504 770 306.</p>
<?php if(!$ajax): ?></div>
<div id="content"><?php endif ?>
	<p>Directeur de la publication : Fabien CROMBEZ</p>
	<p>Le site web est hébergé par l'éditeur.</p>
	<p>Ce site respecte le droit d'auteur. Tous les droits des auteurs des Oeuvres protégées reproduites et communiquées sur ce site, sont réservés. Sauf autorisation, toute utilisation des Oeuvres autres que la reproduction et la consultation individuelles et privées sont interdites.</p>
	<p>Afin d'optimiser l'usage du site web, des cookies peuvent être placé sur les ordinateurs des utilisateurs du site web. Ces cookies sont notamment utilisés pour faciliter l'accès des utilisateurs à leurs comptes ainsi que pour afficher des bandeaux publicitaires adaptés à leur manière de naviguer sur internet. Pour savoir comment refuser, supprimer ces "Cookies" ou être prévenu de leur réception par un message, veuillez consulter la rubrique d'aide de votre navigateur internet.</p>
<?php if(!$ajax): ?></div><?php endif ?>