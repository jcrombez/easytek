<div id="sous-header">
	<img src="<?=asset('images/icone-realisations.png')?>" alt="icone" />
	<h1>Découvrez nos réalisations.</h1>
	<p>
		Cette page vous présente des projets web que nous avons réalisés, parfois entièrement, parfois en partie, que ce soit le développement (php, symfony ou cms), le design ou son intégration (html / css / javascript).<br />
		Pour obtenir plus d’informations sur ce que nous avons réalisé sur chaque projet, passez votre souris sur un site.
	</p>
</div>
<div id="content">
	<?php loadPage('_nomoreie'); ?>
	
	<div id="realisations">
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>OMNT</h3>
					<p class="tags">CodeIgniter - Webdesign - Multilangue</p>
					<p class="description"><span>Description :</span> Site d'actualité et d'information multilingue de l'OMNT (Observatoire des Micro et Nanotechnologies) basé au CEA (Commissariat à l'Energie Atomique).</p>
					<p class="realise"><span>Réalisé :</span><strong> Réalisaion et intégration du design, développement complet avec zone d'administration et espace dédié aux clients.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Code Igniter, PHP, MySQL</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/omnt-small.png')?>" alt="www.OMNT.fr" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Device-ALab</h3>
					<p class="tags">CodeIgniter - Intégration  - Multilangue</p>
					<p class="description"><span>Description :</span> Site vitrine multilingue présentant Device-ALab et ses activités de services.</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni par le client, développement complet avec zone d'administration et espace dédié aux clients.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Code Igniter, PHP, MySQL</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/devicealab-small.png')?>" alt="www.Device-ALab.com" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Electronic Music Diary</h3>
					<p class="tags">Symfony - Webdesign  - Intégration</p>
					<p class="description"><span>Description :</span> Site centralisant les festivals de musiques electroniques partout dans le monde et proposant du covoiturage et des galeries de photos.</p>
					<p class="realise"><span>Réalisé :</span><strong> Création et intégration du design ainsi que développement complet avec Symfony 1.4.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Symfony, PHP, Doctrine, Facebook connect, jQuery</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/emd-small.png')?>" alt="www.ElectronicMusicDiary.com" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>FFeminin</h3>
					<p class="tags">Intégration  - XSLT</p>
					<p class="description"><span>Description :</span> Deux sites d'actualité, le premier destiné aux femmes, le second aux hommes (Mmasculin).</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni par le client.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> XML, XSLT</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/ffeminin-small.png')?>" alt="www.FFeminin.fr" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Code-Promotion</h3>
					<p class="tags">Framework - Intégration  - XSLT - Propel</p>
					<p class="description"><span>Description :</span> Site centralisant les codes de réduction de sites d'e-commerce.</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni par le client, développement complet avec zone d'administration et script d'import xml.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Framework, PHP, Propel, XSLT, jQuery</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/codepromo-small.png')?>" alt="www.Code-Promotion.fr" />
		</div>
<?php /*		<div class="realisation">
			<div class="warper">
				<div class="infos">
					<h3>Creaconcours</h3>
					<p class="tags">Framework - Intégration - Propel</p>
					<p class="description"><span>Description :</span> Site de proposant de créer des jeux concours en ligne.</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni par le client, développement complet avec zone membres et zone d'administration.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Framework, PHP, Propel, jQuery</strong></p>
				</div>
			</div>
			<img src="images/realisations/realisations/creaconcours-small.png" alt="Creaconcours.com" />
		</div> */ ?>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Redirections</h3>
					<p class="tags">PHP - Multilangue</p>
					<p class="description"><span>Description :</span> Annuaire de sites avec un service de domaines de redirection gratuits fournissant des statistiques de visites et de votes.</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni et développement complet du site avec une zone d'administration pour les utilisateurs.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> PHP, MySQL, XSLT</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/redirections-small.png')?>" alt="www.Redirections.fr" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Ligue contre la violence routière</h3>
					<p class="tags">SPIP - Webdesign - Intégration</p>
					<p class="description"><span>Description :</span> Site de la ligue contre la violence routière du Loiret, présente l'association et ses actions.</p>
					<p class="realise"><span>Réalisé :</span><strong> Création et intégration du design, personnalisation et simplification la zone d'administration du CMS qui permet la publication d'articles dans chacune des catégories du site.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> SPIP, PHP, MySQL</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/lcvr45-small.png')?>" alt="www.ViolenceRoutiere45.org" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>Annuaire (mise en ligne à venir)</h3>
					<p class="tags">Framework - Intégration</p>
					<p class="description"><span>Description :</span> Site annuaire décliné en plusieurs thématiques.</p>
					<p class="realise"><span>Réalisé :</span><strong> Intégration du design fourni par le client, développement complet avec zone d'administration et espace dédié aux membres.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> Framework, PHP, Propel, XSLT, jQuery</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/annuaire-small.png')?>" alt="www.AnnuaireJeu.fr" />
		</div>
		<div class="realisation">
			<div class="wraper">
				<div class="infos">
					<h3>PHL Rénovation Loiret</h3>
					<p class="tags">PHP - Webdesign - Intégration</p>
					<p class="description"><span>Description :</span> Site vitrine de PHL, entreprise artisanale de rénovation de maisons.</p>
					<p class="realise"><span>Réalisé :</span><strong> Création et intégration du design, développement complet du site, zone d'administration permettant au client de publier les photos la description des travaux qu'il a réalisés.</strong></p>
					<p class="technologies"><span>Technologies :</span><strong> PHP, MySQL, SMARTY</strong></p>
				</div>
			</div>
			<img src="<?=asset('images/realisations/phl-small.png')?>" alt="www.PHL-Renovation-Loiret.fr" />
		</div>
	</div>
</div>
